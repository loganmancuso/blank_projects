/**
 * 'main.h'
 * Header file that includes the 'Utilities' code.
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 08-22-2019--14:30:49
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
 *
**/

#ifndef MAIN_H
#define MAIN_H

#include <iostream>
using namespace std;

#include "../util/utils.h"
#include "../util/scanner.h"
#include "../util/scanline.h"
#include "dothework.h"

/**
 * Add Code Here:
**/

string in_filename = "XX";
string out_filename = "XX";
string log_filename = "XX";
string log_string = "XX";
string out_string = "XX";
string tim_output = "XX";

//setup an outstream for data to write to log file
std::ofstream out_stream;
Scanner input_stream;
std::ofstream log_stream;



#endif //MAIN_H

/**
 * End 'main.h'
**/