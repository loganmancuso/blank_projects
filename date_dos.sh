#!/bin/bash
today=$(date +%m-%d-%Y--%H:%M:%S) to_replace="Last Edit Date:"

loop() {
for item in *; do
	if [[ -f $item ]]; then
		echo "file" $item
		sed -i "0,\|.* $to_replace.*|{s|$to_replace.*|$to_replace $today|g1}" $item
		dos2unix -q $item
	elif [[ -d $item ]]; then
		echo "dir" $item
		cd $item
		loop
		cd ../
	fi
done
}

for item in *; do
	if [[ -f $item  && $item != "date_dos.sh" ]]; then
		echo "file" $item
		sed -i "0,\|.* $to_replace.*|{s|$to_replace.*|$to_replace $today|g1}" $item
		dos2unix -q $item
	elif [[ -d $item ]]; then
		echo "dir" $item
		cd $item
		loop
		cd ../
	fi
done