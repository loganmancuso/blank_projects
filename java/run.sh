#!/bin/bash
###
# 'run.sh'
# This script will build the program 
# needs to be run from 
# 'ProgramName'/working/src/
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 07-05-2019--14:17:58
###

###
# Variables
###

# Error Messages
WRONG_DIRECTORY="Move To Source Directory To Build Program"
YES_OR_NO="Invalid please type [Y]es or [N]o"
# Top
WORKING_DIR="working"
TEST_DIR=".test"
IOFILES_DIR="iofiles"
# Middle
SRC_DIR="$WORKING_DIR/src"
# Active
SH_LOG="$IOFILES_DIR/sh.log"
BUILD_ERR="$IOFILES_DIR/build.err"
X_IN="$IOFILES_DIR/x.in"
X_OUT="$IOFILES_DIR/x.out"
X_LOG="$IOFILES_DIR/x.log"

###
# this function will refresh the date of last edit in all files
# recursively by sub directory
# finds string "Last Edit Date: 11-29-2017--11:04:36
# the current date and time
###
function zaRefreshDate() {
	today=$(date +%m-%d-%Y--%H:%M:%S)
	to_replace="Last Edit Date:"
	#loop to replace last edit date
	loop() {
		for item in $SRC_DIR/*; do
			if [[ -f $item ]]; then
				echo "File: $item"
				sed -i "0,\|.* ${to_replace}.*|{s|${to_replace}.*|${to_replace} ${today}|g1}" $item
			elif [[ -d $item ]]; then
				echo "Directory: $item"
				cd $item
				loop #recursive call
				cd ../
			else
				echo "Error: $item"
			fi
		done
	}
	# loop for all file in dir
	for item in $SRC_DIR/*; do
		if [[ -f $item ]]; then
			echo "File: $item"
			sed -i "0,\|.* ${to_replace}.*|{s|${to_replace}.*|${to_replace} ${today}|g1}" $item
		elif [[ -d $item ]]; then
			echo "Directory: $item"
			cd $item
			# use recursion if a directory is found
			loop
			cd ../
		else
			echo "Error: $item"
		fi
	done
}

###
# this function will move all files in
# the working directory to the test 
# directory 
###
function zbMigrate() {
	echo "Migrating '$WORKING_DIR' to '$TEST_DIR'"
	mkdir $TEST_DIR
	for item in $WORKING_DIR/*; do
		echo "Copying '${item}' to '$TEST_DIR'"
		cp -r ${item} $TEST_DIR
	done
}

###
# this function will compile the files
# in the test directory 
###
function zcCompile() {
	echo "Descending into $TEST_DIR directory"
	cd .test/src/
	echo "COMPILING"
	javac *.java
	if [ $? -eq 0 ]; then
		echo "Build Successful"
	else
		echo "Build Failed"
	fi
	cd ../../ # move back to main directory 
	echo "COMPILING COMPLETE"
}

function zeExecute() {
	cd $TEST_DIR/src
	echo "EXECUTING"
	java aprog ../../$X_IN ../../$X_OUT ../../$X_LOG
	echo "EXECUTION COMPLETE"
	cd ../../
}

### Begining of the build

# check directory
if [[ $(pwd)/ = *$SRC_DIR/ ]]; then
	echo "Moving out of '$SRC_DIR' Directory"
	cd ../../
	if [[ $(pwd)/ = *$SRC_DIR ]]; then
		echo $WRONG_DIRECTORY
		exit 1
	fi
fi

# File Cleanp
rm -r $SH_LOG $TEST_DIR $BUILD_ERR $X_OUT $X_LOG
touch $SH_LOG $BUILD_ERR $X_IN $X_OUT $X_LOG

#  running every time slows computation
read -p "Refresh Date...? [Y]es / [N]o:    " yn_
case "${yn_}" in
  [yY][eE][sS]|Y|y ) # yes
    echo "Changing Date" | tee -a $SH_LOG
    zaRefreshDate | tee -a $SH_LOG
    ;;
  [nN][oO]|N|n ) # no
    echo "Skipping" | tee -a $SH_LOG
    ;;
  * ) # else
    echo $YES_OR_NO | tee -a $SH_LOG
	exit 1
    ;;
esac
echo "-----------------------------------------------------------------" | tee -a $SH_LOG
zbMigrate | tee -a $SH_LOG
echo "-----------------------------------------------------------------" | tee -a $SH_LOG
zcCompile |& tee -a $BUILD_ERR 
#  run only if build hasnt failed
read -p "Run Program...? [Y]es / [N]o:    " yn_
case "${yn_}" in
  [yY][eE][sS]|Y|y ) # yes
    zeExecute | tee -a $SH_LOG
    ;;
  [nN][oO]|N|n ) # no
    echo "Run Stopped" | tee -a $SH_LOG
	exit 1
    ;;
  * ) # else
    echo $YES_OR_NO | tee -a $SH_LOG
	exit 1
    ;;
esac

echo "Done Building Program" 

###
# End 'run.sh'
###